taxonomy_extends 模块对taxonomy做了一些扩展。

  Token "term:hierarchypath":
    提供一个token, "term:hierarchypath". 按分类里的术语，生成层级的路径，
    形式为：分类名称/根术语名称/下一级术语名称/...
    
    例如一个分类叫做音乐，术语如下：
       中国音乐
         古典音乐
         流行音乐
       英语音乐
    
     "中国音乐"对应的token是"音乐/中国音乐", "古典音乐"的token　是"音乐/中国音乐/古典音乐"
     这个token可以用于pathauto模块生成术语的访问路径。

  节点排序功能:
    这个功能最早设计用于实现“栏目”的功能。
      1. 用一个分类存储栏目名称，并在分类中添加各栏目。假定这个分类的名称是“栏目名称”
      2. 在node字段中添加一个字段，来源于分类“栏目名称”
      3. 在本模块的配置中设置相应的字段需要排序
         3.1 在配置菜单中选择“taxonomy extends config“
         3.2 在“taxonomy extends”菜单中选择“config”
         3.3 路径：admin/config/system/taxonomy_extends
      4. 初始化数据
         4.1 在“taxonomy extends”菜单中选择“initialize”
         4.2 路径： admin/taxonomy_extends/nodeorder/init
      5. 用: 栏目名称/<栏目名称><子栏目名称>/list 这种形式的路径访问节点列表。此列表是排序后的列表。
         5.1 此列表的顶部有置顶排序和节点排序的链接，可分别手动指定置顶顺序或节点顺序
      6. 在菜单“taxonomy extends“中有此模块常用的管理链接，能找到更多的菜单项，可用于theme中。
    
    从此模块的设计思路来看，适合在所有的术语下排序节点。
    
    限制： 同一个节点，不能有两个字段来自于同一个术语。在使用上，请不要在设置节点的两个字段来自于
    同一个术语表。
           
 　 数据存放在表node_term_order(tid,nid,node_order,sticky_order)里。
    排序规则： sticky_order desc, node_order desc.
    新加入某个术语的节点会排在前面。
    
    配置地址：admin/config/system/taxonomy_extends，这里可设置参与排序的分类字段，分类字段的名称存
    放在系统变量taxonomy_extends_nodeorder_fields里。
    
    初始化地址: admin/taxonomy_extends/nodeorder/init，把没有加到node_term_order里的节点
	加入此表中，同时为所有参与排序的术语设置前台显示的路径别名。
    路径名为：token[term:hierarchypath]/list, 
    可以重复执行。
    
    调整排序地址：admin/taxonomy_extends/nodeorder/reset, 对node_term_order表里的节点重新计算
    node_order和sticky_order的值，使之间隔为3。节点间的序号初始相差3。在使用过程中，节点序号被
    调整，可能会出现序号过于密集的情况，此时可用此功能调整。
      
    此模块为views模块提供了三个字段：taxonomy_extends: Node Order， 
	taxonomy_extends: Sticky Order 和 taxonomy_extends: tid。
    排序时，使用taxonomy_extends: Sticky Order desc
	taxonomy_extends: Node Order desc。
    由于此模块提供的排序功能是在某一术语下的排序，所以要同时指定tid。filter中加入
    taxonomy_extends: tid，并指定等于需要排序的term id。term id可以在分类管理中找到。
    
    注意事项：
      每次当node加入一个term时，会从node_term_order里取对应term最大序号+3。因为没有加严格
	  事务管理，所以在大并发量的时候可能会出现序号重复的情况。此时因为节点差不多同时进入系
	  统，其先后顺序并不敏感。
      
      使用界面admin/taxonomy_extends/term/%taxonomy_term/nodeorder拖动调整顺序时，程序会在节点
	  之间调换order值，而不会改变他们。

      在views中使用时，因为node_term_order是按照术语对节点进行排序。当一个节点属于多个术语时，在
      node_term_orde中就会出现多条记录，node_term_order也会视这个节点为多个节点。在views中使用
      node_term_order节点的值进行排序时，就需要同时指定tid。可手工先找到对应术语的值，再在filter
      中录入。
