<?php
/**
 * @file
 * The forms File for taxonomy_extends module, include all forms declaration and
 * the handler
 */


/**
 * Config form.
 */
function taxonomy_extends_config_form($form, &$form_state) {
  $term_fields = variable_get('taxonomy_extends_nodeorder_fields');
  if ($term_fields == NULL) {
    $term_fields = array();
  }
  $query = db_select("field_config_instance", "fci");
  $query->join('field_config', 'fc', 'fci.field_name=fc.field_name');
  $query->fields('fci')
    ->condition('module', 'taxonomy');

  $result = $query->execute();

  if ($result->rowCount() < 1) {
    $form['nocontent'] = array(
      '#markup' => t('no taxonomy fields found'),
    );
  }
  else {

    $options = array();
    while ($row = $result->fetchAssoc()) {
      $fcidata = unserialize($row['data']);
      $options[$row['field_name']] = $fcidata['label'];
    }

    $form['fields'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Please check the fields need to order'),
      '#options' => $options,
      '#default_value' => $term_fields,
    );
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save changes'));
  }
  return $form;
}

/**
 * Submit for taxonomy_extends_config_form.
 */
function taxonomy_extends_config_form_submit($form, &$form_state) {
  $value_fields = $form_state['values']['fields'];
  $term_fields = array();
  foreach ($value_fields as $value) {
    if ($value !== 0) {
      $term_fields[] = $value;
    }
  }

  variable_set('taxonomy_extends_nodeorder_fields', $term_fields);
  drupal_set_message(t("config saved"));
}

/**
 * Node order form.
 *
 * you can drag and drop to order nodes
 */
function taxonomy_extends_term_nodeorder_form($form, &$form_state, $term) {
  $tid = $term->tid;
  $query = db_select('node_term_order', 'nto')->extend('PagerDefault')->limit(10);
  $query->join('node', 'n', 'nto.nid = n.nid');
  $query->fields('n', array('nid', 'title', 'created'))
    ->fields('nto', array('tid', 'node_order'))
    ->condition('nto.tid', $tid)
    ->condition('sticky_order', 0, '<=')
    ->orderBy('sticky_order', 'desc')
    ->orderBy('nto.node_order', 'desc');

  $result = $query->execute();
  $form['term_name'] = array("#markup" => $term->name);
  $form['nodes']['#tree'] = TRUE;
  $form['nodes']['#theme'] = 'theme_taxonomy_extends_term_nodeorder_form';
  $delta = 50 * 5;
  $count = 0;
  $form['foreactions'] = array('#type' => 'actions');
  $form['foreactions']['submit'] = array('#type' => 'submit', '#value' => t('Save changes'));

  foreach ($result as $row) {
    $form['nodes'][$count]['title'] = array(
      '#markup' => l(check_plain($row->title), "node/" . $row->nid),
    );
    $form['nodes'][$count]['created'] = array('#markup' => date("Y-m-d H:i:s", $row->created));
    $form['nodes'][$count]['nid'] = array(
      '#type' => 'value',
      '#title_display' => 'invisible',
      '#value' => $row->nid,
    );
    $form['nodes'][$count]['tid'] = array(
      '#type' => 'value',
      '#title_display' => 'invisible',
      '#value' => $row->tid,
    );

    $form['nodes'][$count]['node_order'] = array(
      '#type' => 'value',
      '#title_display' => 'invisible',
      '#value' => $row->node_order,
    );

    $form['nodes'][$count]['weight'] = array(
      '#type' => 'weight',
      '#delta' => $delta,
      '#title_display' => 'invisible',
      '#default_value' => $count,
      '#title' => t('Weight for @title', array('@title' => $row->title)),
    );
    $count += 1;
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save changes'));

  return $form;
}

/**
 * The theme of node order form.
 */
function theme_taxonomy_extends_term_nodeorder_form($variables) {
  $form = $variables['form'];
  $rows = array();
  if (isset($_GET["page"])) {
    $page = intval($_GET["page"]);
  }
  else {
    $page = 0;
  }

  foreach (element_children($form['nodes']) as $count) {
    $form['nodes'][$count]['weight']['#attributes']['class'] = array('text-format-order-weight');
    $rows[] = array(
      'data' => array(
        drupal_render($form['nodes'][$count]['title']),
        drupal_render($form['nodes'][$count]['created']),
        drupal_render($form['nodes'][$count]['weight']),
        l(t("property"), 'admin/taxonomy_extends/setting/' . $form['nodes'][$count]['tid']['#value'] .
          '/' . $form['nodes'][$count]['nid']['#value'] . '/' .
           base64_encode(request_path()) . "/" . $page),
        drupal_render($form['nodes'][$count]['disable']),
      ),
      'class' => array('draggable'),
    );
  }
  $header = array(
    t('title'),
    t('created'),
    t('weight'),
    array(
      'data' => t('Operations'),
      'colspan' => 2,
    ),
  );
  $output = drupal_render($form['term_name']);
  $output .= drupal_render($form['foreactions']);
  $output .= theme('table', array(
    'header' => $header, 'rows' => $rows, 'attributes' =>
      array('id' => 'text-format-order')));
  $output .= drupal_render_children($form);
  $output .= theme('pager', array('#theme' => 'pager', '#weight' => 5));

  drupal_add_tabledrag('text-format-order', 'order', 'sibling', 'text-format-order-weight');

  return $output;
}

/**
 * Submit of node order form.
 *
 * The data is an array, every element has three item: weight, node_order, nid
 * In the order table, weight from low to high, node order from high to low
 */
function taxonomy_extends_term_nodeorder_form_submit($form, &$form_state) {

  // Var_dump($form_state);
  if (isset($form_state['values']['nodes'])) {
    $arr = $form_state['values']['nodes'];
    // Print_r($arr);
    // Read all node order values.
    $node_order_arr = array();
    for ($i = 0; $i < count($arr); $i++) {
      $node_order_arr[] = $arr[$i]['node_order'];
    }

    arsort($node_order_arr);

    // Order the array by weight.
    for ($i = 0; $i < count($arr); $i++) {
      for ($j = $i + 1; $j < count($arr); $j++) {
        if ($arr[$j]['weight'] < $arr[$i]['weight']) {
          $row = $arr[$j];
          $arr[$j] = $arr[$i];
          $arr[$i] = $row;
        }
      }
    }

    for ($i = 0; $i < count($arr); $i++) {
      if ($arr[$i]['node_order'] != $node_order_arr[$i]) {
        db_update('node_term_order')
          ->fields(array(
            'node_order' => $node_order_arr[$i],
          ))
          ->condition('nid', $arr[$i]['nid'])
          ->condition('tid', $arr[$i]['tid'])
          ->execute();
      }
    }

    drupal_set_message(t("Data Saved"));
  }
  if (isset($_GET["page"])) {
    $page = intval($_GET["page"]);
  }
  else {
    $page = 0;
  }
  if ($page > 0) {
    drupal_goto(request_path(), array('query' => array('page' => $page)));
  }
}

/**
 * Sticky order form.
 */
function taxonomy_extends_term_stickyorder_form($form, &$form_state, $term) {
  $tid = $term->tid;
  $query = db_select('node_term_order', 'nto')->extend('PagerDefault')->limit(50);
  $query->join('node', 'n', 'nto.nid = n.nid');
  $query->fields('n', array('nid', 'title', 'created'))
    ->fields('nto', array('tid', 'sticky_order'))
    ->condition('nto.tid', $tid)
    ->condition('sticky_order', 0, '>')
    ->orderBy('sticky_order', 'desc')
    ->orderBy('nto.node_order', 'desc');

  $result = $query->execute();
  $form['term_name'] = array("#markup" => $term->name);
  $form['nodes']['#tree'] = TRUE;
  $form['nodes']['#theme'] = 'theme_taxonomy_extends_term_stickyorder_form';
  $delta = 50 * 5;
  $count = 0;
  $form['foreactions'] = array('#type' => 'actions');
  $form['foreactions']['submit'] = array('#type' => 'submit', '#value' => t('Save changes'));

  foreach ($result as $row) {
    $form['nodes'][$count]['title'] = array(
      '#markup' => l(check_plain($row->title), "node/" . $row->nid),
    );
    $form['nodes'][$count]['created'] = array(
      '#markup' => date("Y-m-d H:i:s", $row->created));
    $form['nodes'][$count]['nid'] = array(
      '#type' => 'value',
      '#title_display' => 'invisible',
      '#value' => $row->nid,
    );
    $form['nodes'][$count]['tid'] = array(
      '#type' => 'value',
      '#title_display' => 'invisible',
      '#value' => $row->tid,
    );

    $form['nodes'][$count]['sticky_order'] = array(
      '#type' => 'value',
      '#title_display' => 'invisible',
      '#value' => $row->sticky_order,
    );

    $form['nodes'][$count]['weight'] = array(
      '#type' => 'weight',
      '#delta' => $delta,
      '#title_display' => 'invisible',
      '#default_value' => $count,
      '#title' => t('Weight for @title', array('@title' => $row->title)),
    );
    $count += 1;
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save changes'));

  return $form;
}

/**
 * The theme of sticky order form.
 */
function theme_taxonomy_extends_term_stickyorder_form($variables) {
  $form = $variables['form'];
  $rows = array();
  if (isset($_GET["page"])) {
    $page = intval($_GET["page"]);
  }
  else {
    $page = 0;
  }

  foreach (element_children($form['nodes']) as $count) {
    $form['nodes'][$count]['weight']['#attributes']['class'] = array('text-format-order-weight');
    $rows[] = array(
      'data' => array(
        drupal_render($form['nodes'][$count]['title']),
        drupal_render($form['nodes'][$count]['created']),
        drupal_render($form['nodes'][$count]['weight']),
        l(t("property"), 'admin/taxonomy_extends/setting/' . $form['nodes'][$count]['tid']['#value'] .
          '/' . $form['nodes'][$count]['nid']['#value'] . '/' .
          base64_encode(request_path()) . "/" . $page),
        drupal_render($form['nodes'][$count]['disable']),
      ),
      'class' => array('draggable'),
    );
  }
  $header = array(
    t('title'),
    t('created'),
    t('weight'),
    array('data' => t('Operations'), 'colspan' => 2));
  $output = drupal_render($form['term_name']);
  $output .= drupal_render($form['foreactions']);
  $output .= theme('table', array(
    'header' => $header, 'rows' => $rows, 'attributes' =>
    array('id' => 'text-format-order')));
  $output .= drupal_render_children($form);
  $output .= theme('pager', array(
    '#theme' => 'pager', '#weight' => 5));

  drupal_add_tabledrag('text-format-order', 'order', 'sibling', 'text-format-order-weight');

  return $output;
}

/**
 * Submit of sticky order form.
 *
 * the data is an array, every element has three item: weight: sticky_order, nid
 * in the order table, weight from low to high, sticky_order from high to low
 */
function taxonomy_extends_term_stickyorder_form_submit($form, &$form_state) {

  // Var_dump($form_state);
  if (isset($form_state['values']['nodes'])) {
    $arr = $form_state['values']['nodes'];

    // Read all sticky_order
    $sticky_order_arr = array();
    for ($i = 0; $i < count($arr); $i++) {
      $sticky_order_arr[] = $arr[$i]['sticky_order'];
    }

    arsort($sticky_order_arr);

    // Order the array by weight.
    for ($i = 0; $i < count($arr); $i++) {
      for ($j = $i + 1; $j < count($arr); $j++) {
        if ($arr[$j]['weight'] < $arr[$i]['weight']) {
          $row = $arr[$j];
          $arr[$j] = $arr[$i];
          $arr[$i] = $row;
        }
      }
    }

    // Update ...
    for ($i = 0; $i < count($arr); $i++) {
      if ($arr[$i]['sticky_order'] != $sticky_order_arr[$i]) {
        db_update('node_term_order')
          ->fields(array(
            'sticky_order' => $sticky_order_arr[$i],
          ))
          ->condition('nid', $arr[$i]['nid'])
          ->condition('tid', $arr[$i]['tid'])
          ->execute();
      }
    }
    drupal_set_message(t("data saved"));

  }
  if (isset($_GET["page"])) {
    $page = intval($_GET["page"]);
  }
  else {
    $page = 0;
  }
  if ($page > 0) {
    drupal_goto(request_path(), array('query' => array('page' => $page)));
  }
}

/**
 * Node setting form.
 */
function taxonomy_extends_node_setting_form($form, &$form_state, $term, $node) {
  $term_fields = variable_get('taxonomy_extends_nodeorder_fields');
  $found = FALSE;
  foreach ($term_fields as $field_name) {
    $items = field_get_items('node', $node, $field_name);
    foreach ($items as $item) {
      if ($item['tid'] == $term->tid) {
        $found = TRUE;
        break;
      }
    }
  }

  $args = arg();

  if (isset($args[5])) {
    if (isset($args[6])) {
      $page = $args[6];
      $pagearray = array("query" => array("page" => check_plain($page)));
    }
    else {
      $pagearray = array();
    }
    $form['gotonodeorder'] = array(
      '#markup' => l(t("return"), base64_decode($args[5]), $pagearray),
    );
  }

  if ($found) {
    $form['nid'] = array(
      "#type" => 'value',
      '#value' => $node->nid,
    );

    $form['tid'] = array(
      "#type" => 'value',
      '#value' => $term->tid,
    );

    $form['node_title'] = array('#markup' => '<div>' . t('Title') . ':' . check_plain($node->title) . "</div>");
    $form['term_name'] = array('#markup' => '<div>' . t('Term Name') . ':' . check_plain($term->name) . "</div>");

    $result = db_select("node_term_order", "nto")
      ->fields("nto", array("sticky_order", "node_order"))
      ->condition("nid", $node->nid)
      ->condition("tid", $term->tid)
      ->execute();
    $row = $result->fetchAssoc();
    if ($row) {
      $sticky_order = $row['sticky_order'];
      // Sticky position.
      $node_order = $row['node_order'];
      if ($sticky_order > 0) {
        $sticky_pos = db_query("select count(*) from {node_term_order} where
          tid = :tid and sticky_order > :sticky_order",
          array(
            ":tid" => $term->tid,
            ":sticky_order" => $sticky_order,
          )
        )->fetchField();
        $sticky_pos++;
      }
      else {
        $sticky_pos = 0;
      }

      if ($sticky_order < 1) {
        $node_pos = db_query("select count(*) from {node_term_order} where tid=:tid and node_order >
            :node_order and sticky_order<1",
            array(
              ":tid" => $term->tid,
              ":node_order" => $node_order,
            )
          )->fetchField();
        $node_pos++;
      }
      else {
        $node_pos = 0;
      }
    }
    else {
      $sticky_pos = 0;
      $node_pos = 1;
    }

    if (taxonomy_extends_api_sticky_order_right($term)) {

      $form['sticky_pos_old'] = array(
        '#type' => 'value',
        '#value' => $sticky_pos,
      );

      $form['sticky_pos'] = array(
        "#type" => 'textfield',
        '#size' => 10,
        '#maxlength' => 10,
        '#title' => t('sticky order'),
        '#default_value' => $sticky_pos,
        "#description" => t("sticky order, 0 to cancel sticky"),
      );
    }

    if (taxonomy_extends_node_order_right($term)) {

      $form['node_pos_old'] = array(
        '#type' => 'value',
        '#value' => $node_pos,
      );

      $form['node_pos'] = array(
        "#type" => 'textfield',
        '#size' => 10,
        '#title' => t('node order'),
        '#default_value' => $node_pos,
        "#description" => t("node order"),
      );
    }

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save changes'));

  }
  else {
    $form['notmatch'] = array('#markup' => t('node and term not match'));
  }

  return $form;
}

/**
 * Submit of node setting form.
 */
function taxonomy_extends_node_setting_form_submit($form, &$form_state) {
  // Print_r($form_state["values"]);
  $batchsize = 10;
  $values = $form_state["values"];
  $nid = $values["nid"];
  $tid = $values["tid"];
  $sticky_pos = $values["sticky_pos"];
  $node_pos = $values["node_pos"];
  $sticky_pos_old = $values['sticky_pos_old'];
  $node_pos_old = $values['node_pos_old'];

  if (empty($nid) || empty($tid)) {
    return;
  }

  $term = taxonomy_term_load($tid);

  if (!taxonomy_extends_api_sticky_order_right($term)) {
    $sticky_pos = NULL;
  }
  if (!taxonomy_extends_node_order_right($term)) {
    $node_pos = NULL;
  }

  if (($sticky_pos == NULL) && ($node_pos == NULL)) {
    return;
  }

  $node = node_load($nid);

  $term_fields = variable_get('taxonomy_extends_nodeorder_fields');
  $found = FALSE;
  foreach ($term_fields as $field_name) {
    $items = field_get_items('node', $node, $field_name);
    foreach ($items as $item) {
      if ($item['tid'] == $tid) {
        $found = TRUE;
        break;
      }
    }
  }

  if (!$found) {
    // Term not found.
    drupal_set_message(t("term not found"));
    return;
  }

  if ($sticky_pos != NULL) {
    if ($sticky_pos > 0 && $sticky_pos != $sticky_pos_old) {
      // When adjusted the sticky position, update database ...
      if ($sticky_pos > $sticky_pos_old && $sticky_pos_old > 0) {
        $sticky_pos++;
        // New position > old position.
      }

      // Find the node in the position of sticky_pos.
      // The sticky_pos value bigger than this node.
      $query = db_select("node_term_order", "nto")
        ->fields('nto')
        ->condition('tid', $tid)
        ->condition('sticky_order', 0, '>')
        ->orderBy('sticky_order', 'desc')
        ->range($sticky_pos - 1, 1);
      $result = $query->execute();

      if ($row = $result->fetchAssoc()) {
        if ($sticky_pos == 1) {
          $_sticky_order = $row["sticky_order"] + 3;
        }
        else {
          $_sticky_order = $row["sticky_order"] + 1;
        }
      }
      else {
        if ($sticky_pos == 1) {
          $_sticky_order = 3;
        }
        else {
          $_sticky_order = 1;
        }
      }

      // This array stores the rows needs be updated.
      $ntoupdate = array();
      array_push($ntoupdate, array(
        'nid' => $nid,
        'tid' => $tid,
        'order' => $_sticky_order));

      // Every node that order<=$_sticky_order needs to be updated.
      $order_high = $_sticky_order;
      while ($order_high <= $_sticky_order) {
        $query = db_select("node_term_order", "nto")
          ->fields('nto')
          ->condition('tid', $tid)
          ->condition('sticky_order', $order_high, '>=')
          ->orderBy('sticky_order', 'asc')
          ->range(0, $batchsize);
        $result = $query->execute();

        $node_counter = $result->rowCount();
        while ($row = $result->fetchAssoc()) {
          $order_high = $row['sticky_order'];
          if ($order_high <= $_sticky_order) {
            $_sticky_order++;
            array_push($ntoupdate, array(
              'nid' => $row['nid'],
              'tid' => $row['tid'],
              'order' => $_sticky_order));
          }
          else {
            break;
          }
        }

        if ($node_counter < $batchsize) {
          break;
          // If rows received less than batchsize, no more rows, exit.
        }
      }

      // Update database.
      foreach ($ntoupdate as $nto) {

        db_update("node_term_order")
          ->fields(array(
            "sticky_order" => $nto['order'],
          ))
          ->condition('tid', $nto['tid'])
          ->condition('nid', $nto['nid'])
          ->execute();
      }

      if ($node->sticky < 1) {
        $node->sticky = 1;
        node_save($node);
      }

    }
    elseif ($sticky_pos < 1 and $sticky_pos_old > 0) {

      $node->sticky = 0;
      node_save($node);

      db_update('node_term_order')
        ->fields(array(
          'sticky_order' => 0,
          ))
        ->condition('nid', $node->nid)
        ->condition('tid', $tid)
        ->execute();
    }
  }

  if ($node_pos != NULL and $node_pos >= 0 and $node->sticky < 1) {
    if ($node_pos != $node_pos_old) {
      // When sdjusted the node position, update database.
      if ($node_pos > $node_pos_old) {
        $node_pos++;
        // New position below old position.
      }

      $query = db_select("node_term_order", "nto")
        ->fields('nto')
        ->condition('tid', $tid)
        ->condition('sticky_order', 0, '<=')
        ->orderBy('node_order', 'desc')
        ->range($node_pos - 1, 1);
      $result = $query->execute();
      // Find the node in the position of node_pos, the node_pos value
      // of current node is Bigger than this node.
      if ($row = $result->fetchAssoc()) {
        if ($node_pos == 1) {
          $_node_order = $row["node_order"] + 3;
        }
        else {
          $_node_order = $row["node_order"] + 1;
        }
      }
      else {
        if ($node_pos == 1) {
          $_node_order = 3;
        }
        else {
          $_node_order = 1;
        }
      }

      $ntoupdate = array();
      array_push($ntoupdate, array(
        'nid' => $nid,
        'tid' => $tid,
        'order' => $_node_order));

      // Every node of order<=$_node_order needs be updated.
      $order_high = $_node_order;
      while ($order_high <= $_node_order) {
        $query = db_select("node_term_order", "nto")
          ->fields('nto')
          ->condition('tid', $tid)
          ->condition('node_order', $order_high, '>=')
          ->orderBy('node_order', 'asc')
          ->range(0, $batchsize);
        $result = $query->execute();

        $node_counter = $result->rowCount();
        while ($row = $result->fetchAssoc()) {
          $order_high = $row['node_order'];
          if ($order_high <= $_node_order) {
            $_node_order++;
            array_push($ntoupdate, array(
              'nid' => $row['nid'],
              'tid' => $row['tid'],
              'order' => $_node_order));
          }
          else {
            break;
          }
        }

        if ($node_counter < $batchsize) {
          break;
          // If rows received less that batchsize, no more rows, exit .
        }
      }

      // Update database.
      foreach ($ntoupdate as $nto) {

        db_update("node_term_order")
          ->fields(array(
            "node_order" => $nto['order'],
          ))
          ->condition('tid', $nto['tid'])
          ->condition('nid', $nto['nid'])
          ->execute();
      }
    }
  }

  drupal_set_message(t("Data Saved"));
}
