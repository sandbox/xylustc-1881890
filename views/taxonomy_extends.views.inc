<?php
/**
 * @file
 * The views data File for taxonomy_extends module
 */

/**
 * Implements hook_views_data().
 */
function taxonomy_extends_views_data() {
  $data['node_term_order']['table']['group'] = 'taxonomy_extends';

  $data['node_term_order']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  // Node ID field.
  $data['node_term_order']['nid'] = array(
    'title' => 'nid',
    'help' => t('node_term_order.nid'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('nid'),
    ),
  );

  // Taxonomy term ID field.
  $data['node_term_order']['tid'] = array(
    'title' => 'tid',
    'help' => t('node_term_order.tid'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  $data['node_term_order']['sticky_order'] = array(
    'title' => t("Sticky Order"),
    'help' => t('When the value > 0, the node is sticky. Descend order.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  $data['node_term_order']['node_order'] = array(
    'title' => t("Node Order"),
    'help' => t('Node order, descend.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  return $data;
}
