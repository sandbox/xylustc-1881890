<?php
/**
 * @file
 * The Api File for taxonomy_extends module
 */

/**
 * Api for right of access taxonomy_extends config form.
 *
 * User with "administrator nodes" and "bypass node access" right can 
 * access taxonomy_extends module's config form.
 * You script can implements this api to change the right.
 */
function hook_taxonomy_extends_config_right_alter($term) {
  if (user_access('administer nodes') || user_access('bypass node access')) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Api for right of change sticky node's order.
 *
 * User with "administrator nodes" and "bypass node access" right can 
 * change sticky order.
 * You script can implements this api to change the right.
 */
function hook_taxonomy_extends_sticky_order_right_alter($term) {
  if (user_access('administer nodes') || user_access('bypass node access')) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Api for right of change node order.
 *
 * User with "administrator nodes" and "bypass node access" right can 
 * change node order.
 * You script can implements this api to change the right.
 */
function hook_taxonomy_extends_node_order_right_alter($term) {
  if (user_access('administer nodes') || user_access('bypass node access')) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Api for right of access node setting form in taxonomy extends module.
 *
 * In the node setting form, can set the node order number and sticky 
 * order number
 * User with "administrator nodes" and "bypass node access" right can do it.
 * You script can implements this api to change the right.
 */
function hook_taxonomy_extends_node_setting_right_alter($node, $term) {
  if (user_access('administer nodes') || user_access('bypass node access')) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}
