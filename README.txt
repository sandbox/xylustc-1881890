CONTENTS OF THIS FILE
---------------------

* Introduction
* Features
* Installation
* Usage
* Recommended Modules
* Obtaining Help

INTRODUCTION
------------

Current Maintainer: xylustc
Profile: http://drupal.org/user/2441328
Contact: http://drupal.org/user/2441328/contact
Module Project Page: http://drupal.org/sandbox/xylustc/1881890

This module provides some extends for core taxonomy module. 

FEATURES
--------

1. Provide a Token "term:hierarchypath":
     This is a token, generates hierarchy path like 
     voculaburyname/roottermname/nextleveltermname/...
	 
	 For a voculabury named Music, terms like:
	   ChineseMusic
	     ClassicalMusic
	     POPMusic
	   EnglishMusic
	
     The token for term "ChineseMusic" is "Music/ChineseMusic", and token for 
	 term "ClassicalMusic" is "Music/ChineseMusic/ClassicalMusic"
	 
	 This is useful for pathauto module to generate path alias for term. 
	 
2. node order functionality:
     Order nodes under a term. New nodes appears in the top, you can drag and 
	 drop to order nodes manually, rearrange sticky orders.
	 
	 Visit nodes list at "voculaburyname/roottermname/nextleveltermname/.../list".

     After enabled this module, you can find a menu named "taxonomy extends", it 
	 is the first page of this module. In this page, you can do most task of this 
	 module.
	 
	 The table node_term_order (tid,nid,node_order,sticky_order) stores the order 
	 data. Order Rule: sticky_order desc, node_order desc



INSTALLATION
------------

1. Download and extract the module to [yourdrupalroot]/sites/all/modules
(final structure will be [yourdrupalroot]/sites/all/modules/taxonomy_extends

2. Login as administrator, goto management menu "modules", find module "taxonomy_extends" and enable it.

USAGE
-----

1. First, goto management menu "config", find "taxonomy_extends config" item. This is the place where you
   do some configs about this module. The direct path is "admin/config/system/taxonomy_extends".
   
   Here, you can config which fields should be ordered.
   
2. After you enabled the module, you have a management menu "taxonomy extends", here, also has a "config" link,
   links to the config form.
   
3. After you have config done, you can "initialize" the module from "taxonomy extends" menu.

4. Also, some "manage: <field_name>" links apear in the "taxonomy extends" panel, you can click them to list the
   vocabulary terms, and order nodes under these terms.
